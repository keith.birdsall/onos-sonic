#!/bin/bash
cat <<EOF > /etc/salt/minion
master: 192.168.4.25
EOF
systemctl restart salt-minion

config interface ip add Loopback0 10.255.255.201/32
config interface startup Loopback0
config interface ip add Ethernet0 10.0.40.1/30
config interface startup Ethernet0
config interface ip add Ethernet4 10.0.50.1/30
config interface startup Ethernet4
config save -y
# vtysh -c "
# conf t
# router bgp 65000
#  bgp router-id 10.255.255.201
#  bgp log-neighbor-changes
#  no bgp ebgp-requires-policy
#  no bgp default ipv4-unicast
#  no bgp network import-check
#  neighbor evpn peer-group
#  neighbor evpn remote-as 65000
#  neighbor 10.0.40.2 peer-group evpn
#  neighbor 10.0.50.2 peer-group evpn
#  !
#  address-family ipv4 unicast
#   redistribute connected
#   neighbor evpn activate
#   neighbor evpn route-reflector-client
#   maximum-paths ibgp 4
#  exit-address-family
#  !
#  address-family l2vpn evpn
#   neighbor evpn activate
#   neighbor evpn route-reflector-client
#   advertise ipv4 unicast
#  exit-address-family
# exit
# !
# end
# exit"
