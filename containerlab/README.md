# ONOS Sonic lab
Install a linux vm and full topology for sonic containerlab.
This can be run on mac-os M1 or Intel

### Installation
- sudo brew install multipass
- multipass list
- multipass launch docker

### Next
- multipass shell docker
- sudo apt-get install make
- sudo apt-get install net-tools
- sudo ifconfig enp0s1 mtu 900
- bash -c "$(curl -sL https://get.containerlab.dev)"
- git clone https://gitlab.com/keith.birdsall/onos-sonic.git
- cd onos-sonic
- cd containerlab
- sudo make sonic
- make fix-nic
- make install-apps

## Next steps
Before you run the last make command: go into the dir
/onos-sonic/containerlab/config/sonic/leaf01
sudo nano run_commands.sh. you will need to put in
the IP address of our pc that is running the nsp code/salt
master. So ifconfig and use your nic's IP address and save the
file !! next >>
- make init-commands

### To connect to a leaf or spine
- make leaf01
- make leaf02
- make spine01
- make spine02